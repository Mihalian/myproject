"""
Функция print_nth_symbols.

Принимает строку и натуральное число n (целое число > 0).
Вывести символы с индексом n, n*2, n*3 и так далее.
Пример: string='123456789qwertyuiop', n = 2 => result='3579wryip'

Если число меньше или равно 0, то вывести строку 'Must be > 0!'.
Если тип n не int, то вывести строку 'Must be int!'.

Если n больше длины строки, то вывести пустую строку.
"""


def print_nth_symbols(a, b):
    if type(b) != int:
        print("Must be int!")
    elif b <= 0:
        print('Must be > 0!')
    elif b > len(a):
        print("")
    else:
        print(a[b::b]) #подсмотрел у Ксении.гениально



print_nth_symbols("123456789qwertyuio", 4)
