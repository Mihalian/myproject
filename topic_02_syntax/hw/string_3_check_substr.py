"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""


def check_substr(str1, str2):
    a = len(str1)
    b = len(str2)
    if (a < b and (str1 in str2 or str2 in str1)) or (a > b and (str1 in str2 or str2 in str1)):
        return True
    elif str1 == str2:
        return False
    elif a <= 0 or b <= 0:
        return False
    else:
        return False


print(check_substr("1234", " "))
