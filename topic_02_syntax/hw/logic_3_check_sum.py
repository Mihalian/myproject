"""
Функция check_sum.

Принимает 3 числа.
Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
"""


def check_sum(a, b, c):
    if a + b == c or a+c == b or b+c == a:
        return True
    else:
        return False


print(check_sum(1, 1, 2))
