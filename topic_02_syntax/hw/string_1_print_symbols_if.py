"""
Функция print_symbols_if.

Принимает строку.

Если строка нулевой длины, то вывести строку "Empty string!".

Если длина строки больше 5, то вывести первые три символа и последние три символа.
Пример: string='123456789' => result='123789'

Иначе вывести первый символ столько раз, какова длина строки.
Пример: string='345' => result='333'
"""


def print_symbols_if(n):
    if len(n) <= 0:
        print('Empty string!')
    elif len(n) > 5:
        print(n[0:3]+ n[-3:])
    else:
        print(n[0] * len(n))


print_symbols_if("4")