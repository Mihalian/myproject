"""
Функция read_str_from_file.

Принимает 1 аргумент: строка (название файла или полный путь к файлу).

Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен каким-то текстом.)
"""

import json


def read_str_from_file(s):
    with open(s, 'r')as d:
        for i in d:
            print(i)


if __name__ == '__main__':
    res = "di.txt"
    read_str_from_file(res)
