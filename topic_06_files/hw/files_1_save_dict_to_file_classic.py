"""
Функция save_dict_to_file_classic.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Проверить, что записалось в файл.
"""


def save_dict_to_file_classic(s: str, d: dict):
    with open(s, "w") as i:
        i.write(str(d))


print(save_dict_to_file_classic("newf", {"a": "1", "b": "2", "c": "3"}))
