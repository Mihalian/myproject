"""
Функция save_dict_to_file_pickle.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
"""

import pickle


def save_dict_to_file_pickle(s: str, d: dict):
    with open(s, "wb") as i:
        pickle.dump(d, i)

print(save_dict_to_file_pickle("newf2", {"a": "1", "b": "2", "c": "3"}))
