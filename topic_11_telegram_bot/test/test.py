import telebot

from telebot import types

token = "1872348718:AAFKWXI145ltGlYTEflfUW9URp6CpVtjOQU"

bot = telebot.TeleBot(token)

yes_no_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
yes_no_keyboard.row("Да", "Нет")
hideBoard = types.ReplyKeyboardRemove()


@bot.message_handler(commands=['start'], content_types=['text'])
def start(message):
    bot.send_message(message.from_user.id, "Я могу помочь выявить диструктивные паттерны в отношениях. "
                                           "Просто напиши мне ответы на следующие вопросы "
                                           "(первое, что приходит в голову)")
    bot.send_message(message.from_user.id, "Согласен?", reply_markup=yes_no_keyboard)

    # следующий шаг
    bot.register_next_step_handler(message, get_care_cons)
