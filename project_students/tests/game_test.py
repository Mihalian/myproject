from project_students.gam2.game import Gamer
from project_students.gam2.game_state import GameState
from project_students.gam2.game_type import GameType
from project_students.gam2.game_types_weaknesses import game_defence_weaknesses_by_type as weaknesses_by_type


class TestGameClass:
    game_name = 'Бухгалтер кассир'
    game_type = GameType.BOOKKEEPING
    max_hp = 100

    def test_init(self):
        game_test = Gamer(name=self.__class__.game_name,
                          game_type=self.__class__.game_type)

        assert game_test.name == self.__class__.game_name
        assert game_test.game_type == self.__class__.game_type
        assert game_test.weaknesses == weaknesses_by_type[self.__class__.game_type]
        assert game_test.hp == self.__class__.max_hp
        assert game_test.attack_point is None
        assert game_test.defence_point is None
        assert game_test.hit_power == 25
        assert game_test.state == GameState.READY

    def test_str(self):
        pokemon_test = Gamer(name=self.__class__.game_name,
                             game_type=self.__class__.game_type)
        assert str(pokemon_test) == f"Name: {self.__class__.game_name} | " \
                                    f"Type: {self.__class__.game_type.name}\n" \
                                    f"HP: {self.__class__.max_hp}"
