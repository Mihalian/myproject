from project_students.gam2.game_type import GameType
from project_students.gam2.game_types_weaknesses import game_defence_weaknesses_by_type as weaknesses_by_type
from project_students.gam2.body_part import BodyPart
from project_students.gam2.game_state import GameState


class Gamer:
    def __init__(self,
                 name: str,
                 game_type: GameType):
        self.name = name
        self.game_type = game_type
        self.weaknesses = weaknesses_by_type.get(game_type, tuple())
        self.hp = 100
        self.attack_point = None
        self.defence_point = None
        self.hit_power = 25
        self.state = GameState.READY

    def __str__(self):
        return f"Name: {self.name} | Type: {self.game_type.name}\nHP: {self.hp}"

    def next_step_points(self,
                         next_attack: BodyPart,
                         next_defence: BodyPart):
        self.attack_point = next_attack
        self.defence_point = next_defence

    def get_hit(self,
                opponent_attack_point: BodyPart,
                opponent_hit_power: int,
                opponent_type: GameType):
        if opponent_attack_point == BodyPart.NOTHING:
            return "Неплохая попытка!"
        elif self.defence_point == opponent_attack_point:
            return "Тебе понадобиться чтото еще, если хочешь от меня избавиться"
        else:
            self.hp -= opponent_hit_power * (3 if opponent_type in self.weaknesses else 1)

            if self.hp <= 0:
                self.state = GameState.DEFEATED
                return "Пиши заявление!"
            else:
                return "Посмотрим, кто кого"

