from project_students.gam2.game_type import GameType

game_by_type = {
    GameType.HR: {'HR бизнес-партнер': 'hr1.jpg',
                  'HR рекрутер-ресечер': 'hr2.jpg'},

    GameType.BOOKKEEPING: {'Бухгалтер кассир': 'b1.jpg',
                           'Бухгалтер по взаиморасчетам': 'b2.jpg'},


    GameType.ADMINISTRARIVEDEPARTAMENT: {'Служба охраны труда': 'a1.jpg',
                                         'Техническая служба': 'a2.jpg'}
}
