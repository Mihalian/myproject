from enum import Enum, auto


class GameState(Enum):
    READY = auto()
    DEFEATED = auto()
