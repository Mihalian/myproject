import random

from project_students.gam2.game import Gamer
from project_students.gam2.game_type import GameType
from project_students.gam2.game_by_type import game_by_type
from project_students.gam2.body_part import BodyPart


class GamerNPC(Gamer):
    def __init__(self):
        rand_type_value = random.randint(GameType.min_value(), GameType.max_value())
        rand_game_type = GameType(rand_type_value)

        rand_game_name = random.choice(list(game_by_type.get(rand_game_type, {}).keys()))

        super().__init__(rand_game_name, rand_game_type)

    def next_step_points(self, **kwargs):
        attack_point = BodyPart(random.randint(BodyPart.min_value(), BodyPart.max_value()))
        defence_point = BodyPart(random.randint(BodyPart.min_value(), BodyPart.max_value()))
        super().next_step_points(next_attack=attack_point,
                                 next_defence=defence_point)




