from enum import Enum, auto


class GameType(Enum):
    HR = auto()
    BOOKKEEPING = auto()
    ADMINISTRARIVEDEPARTAMENT = auto()


    @classmethod
    def min_value(cls):
        return cls.HR.value

    @classmethod
    def max_value(cls):
        return cls.ADMINISTRARIVEDEPARTAMENT.value
