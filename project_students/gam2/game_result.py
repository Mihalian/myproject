from enum import Enum, auto


class GameResult(Enum):
    W = auto()
    L = auto()
    E = auto()

