import json

import telebot
from telebot import types
from telebot.types import Message

from project_students.gam2.body_part import BodyPart
from project_students.gam2.game_result import GameResult
from project_students.gam2.game import Gamer
from project_students.gam2.game_npc import GamerNPC
from project_students.gam2.game_by_type import game_by_type
from project_students.gam2.game_state import GameState
from project_students.gam2.game_type import GameType

with open("./bot_token.txt", 'r') as token_file:
    token = token_file.read().strip()

bot = telebot.TeleBot(token)

state = {}

statistics = {}

stat_file = "game_stat.json"

body_part_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                               one_time_keyboard=True,
                                               row_width=len(BodyPart))

body_part_keyboard.row(*[types.KeyboardButton(body_part.name) for body_part in BodyPart])


def update_save_stat(chat_id, result: GameResult):
    print("Обновление статистики", end="...")
    global statistics

    chat_id = str(chat_id)

    if statistics.get(chat_id, None) is None:
        statistics[chat_id] = {}

    if result == GameResult.W:
        statistics[chat_id]['W'] = statistics[chat_id].get('W', 0) + 1
    elif result == GameResult.L:
        statistics[chat_id]['L'] = statistics[chat_id].get('L', 0) + 1
    elif result == GameResult.E:
        statistics[chat_id]['E'] = statistics[chat_id].get('E', 0) + 1
    else:
        print(f"Не существует результата {result}")

    with open(stat_file, 'w') as file:
        json.dump(statistics, file)

    print("завершено!")


def load_stat():
    print('Загрузка статистики...', end='')
    global statistics

    try:
        with open(stat_file, 'r') as file:
            statistics = json.load(file)
        print('завершена успешно!')
    except FileNotFoundError:
        statistics = {}
        print('файл не найден!')


@bot.message_handler(commands=["help", "info"])
def help_command(message):
    bot.send_message(message.chat.id,
                     "Привет, добро пожаловать в Корпоративные войны! Тебе предстоит слить своих коллег, тем самым "
                     "заработать повышение. Удачи!\n Нажми /start для того чтобы приступить к корпвойне\n Нажми /stat "
                     "для отображения "
                     "статистики")


@bot.message_handler(commands=["stat"])
def stat(message):
    global statistics
    if statistics.get(str(message.chat.id), None) is None:
        user_stat = "Еще не было ни одной игры"
    else:
        user_stat = "Твои результаты:"
        for res, num in statistics[str(message.chat.id)].items():
            user_stat += f"\n{res}: {num}"

    bot.send_message(message.chat.id,
                     text=user_stat)


@bot.message_handler(commands=["start"])
def start(message):
    yes_no_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                one_time_keyboard=True,
                                                row_width=2)
    yes_no_keyboard.row("Да, погнали", "Нет, мне не нужно повышение")

    bot.send_message(message.from_user.id,
                     text="Пора кого-нибудь слить?",
                     reply_markup=yes_no_keyboard)

    bot.register_next_step_handler(message, start_question_handler)


def start_question_handler(message):
    if message.text.lower() == 'да, погнали':
        bot.send_message(message.from_user.id,
                         'Отлично приступим!')

        create_npc(message)

        ask_user_about_game_type(message)

    elif message.text.lower() == 'нет, мне не нужно повышение':
        bot.send_message(message.from_user.id,
                         'Хм, жаль.')
    else:
        bot.send_message(message.from_user.id,
                         'Я не знаю такого варианта ответа!')


def create_npc(message):
    print(f"Начало создания объекта NPC для chat id = {message.chat.id}")
    global state
    game_npc = GamerNPC()
    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['game_npc'] = game_npc

    npc_image_filename = game_by_type[game_npc.game_type][game_npc.name]
    bot.send_message(message.chat.id, 'Твой оппонент:')
    with open(f"../images/{npc_image_filename}", 'rb') as file:
        bot.send_photo(message.chat.id, file, game_npc)
    print(f"Завершено создание объекта NPC для chat id = {message.chat.id}")


def ask_user_about_game_type(message):
    markup = types.InlineKeyboardMarkup()

    for game_type in GameType:
        markup.add(types.InlineKeyboardButton(text=game_type.name,
                                              callback_data=f"game_type_{game_type.value}"))

    bot.send_message(message.chat.id, "Выбери отдел:", reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "game_type_" in call.data)
def game_type_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) != 3 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Возникла проблема. Перезапусти сессию!")
    else:
        game_type_id = int(call_data_split[2])

        bot.send_message(call.message.chat.id, "Выбери должность:")

        ask_user_about_game_by_type(game_type_id, call.message)


def ask_user_about_game_by_type(game_type_id, message):
    game_type = GameType(game_type_id)
    game_dict_by_type = game_by_type.get(game_type, {})

    for game_name, game_img in game_dict_by_type.items():
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(text=game_name,
                                              callback_data=f"game_name_{game_type_id}_{game_name}"))
        with open(f"../images/{game_img}", 'rb') as file:
            bot.send_photo(message.chat.id, file, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "game_name_" in call.data)
def game_name_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) != 4 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Возникла проблема. Перезапусти сессию!")
    else:
        game_type_id, game_name = int(call_data_split[2]), call_data_split[3]

        create_user_game(call.message, game_type_id, game_name)

        bot.send_message(call.message.chat.id, "Время идти по головам!")

        game_next_step(call.message)


def create_user_game(message, game_type_id, game_name):
    print(f"Начало создания объекта Gamer для chat id = {message.chat.id}")
    global state
    user_game = Gamer(name=game_name,
                      game_type=GameType(game_type_id))

    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['user_game'] = user_game

    image_filename = game_by_type[user_game.game_type][user_game.name]
    bot.send_message(message.chat.id, 'Ты выбрал:')
    with open(f"../images/{image_filename}", 'rb') as file:
        bot.send_photo(message.chat.id, file, user_game)

    print(f"Завершено создание объекта Gamer для chat id = {message.chat.id}")


def game_next_step(message: Message):
    bot.send_message(message.chat.id,
                     "Что будем прикрывать?",
                     reply_markup=body_part_keyboard)

    bot.register_next_step_handler(message, reply_defend)


def reply_defend(message: Message):
    if not BodyPart.has_item(message.text):
        bot.send_message(message.chat.id, "Необходимо выбрать вариант на клавиатуре!")
        game_next_step(message)
    else:
        bot.send_message(message.chat.id,
                         "Что накапал и/или покажешь начальнику?",
                         reply_markup=body_part_keyboard)

        bot.register_next_step_handler(message, reply_attack, defend_body_part=message.text)


def reply_attack(message: Message, defend_body_part: str):
    if not BodyPart.has_item(message.text):
        bot.send_message(message.chat.id, "Необходимо выбрать вариант на клавиатуре!")
        game_next_step(message)
    else:
        attack_body_part = message.text

        global state
        # обращение по ключу ко вложенному словарю для получения объекта покемона пользователя
        user_game = state[message.chat.id]['user_game']

        # обращение по ключу ко вложенному словарю для получения объекта покемона npc
        game_npc = state[message.chat.id]['game_npc']

        # конвертировать строку хранящуюся в объекте attack_body_part в правильный тип enum BodyPart
        # конвертировать строку хранящуюся в объекте defend_body_part в правильный тип enum BodyPart
        user_game.next_step_points(next_attack=BodyPart[attack_body_part],
                                   next_defence=BodyPart[defend_body_part])

        game_npc.next_step_points()

        game_step(message, user_game, game_npc)


def game_step(message: Message, user_game: Gamer, game_npc: Gamer):
    comment_npc = game_npc.get_hit(opponent_attack_point=user_game.attack_point,
                                   opponent_hit_power=user_game.hit_power,
                                   opponent_type=user_game.game_type)
    bot.send_message(message.chat.id, f"NPC game: {comment_npc}\nHP: {game_npc.hp}")

    comment_user = user_game.get_hit(opponent_attack_point=game_npc.attack_point,
                                     opponent_hit_power=game_npc.hit_power,
                                     opponent_type=game_npc.game_type)
    bot.send_message(message.chat.id, f"Your gamer: {comment_user}\nHP: {user_game.hp}")

    if game_npc.state == GameState.READY and user_game.state == GameState.READY:
        bot.send_message(message.chat.id, "Начальник размышляет!")
        game_next_step(message)
    elif game_npc.state == GameState.DEFEATED and user_game.state == GameState.DEFEATED:
        bot.send_message(message.chat.id, "Ничья!")
        update_save_stat(message.chat.id, GameResult.E)
    elif game_npc.state == GameState.DEFEATED:
        bot.send_message(message.chat.id, "Тебя повысили!")
        update_save_stat(message.chat.id, GameResult.W)
    elif user_game.state == GameState.DEFEATED:
        bot.send_message(message.chat.id, "Ты уволен!")
        update_save_stat(message.chat.id, GameResult.L)


if __name__ == '__main__':
    load_stat()

    print('Starting the bot...')
    bot.polling(none_stop=True, interval=0)
    print('The bot has stopped!')
