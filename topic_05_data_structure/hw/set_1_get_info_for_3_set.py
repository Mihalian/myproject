"""
Функция get_info_for_3_set.

Принимает 3 аргумента: множества my_set_left, my_set_mid и my_set_right.

Возвращает dict с информацией:
{
'left == mid == right': True/False,
'left == mid': True/False,
'left == right': True/False,
'mid == right': True/False,

'left & mid': set(...), # intersection
'left & right': set(...),   # intersection
'mid & right': set(...),# intersection

'left <= mid': True/False, # issubset
'mid <= left': True/False, # issubset
'left <= right': True/False,   # issubset
'right <= left': True/False,   # issubset
'mid <= right': True/False,# issubset
'right <= mid': True/False # issubset
}

Если вместо множеств передано что-то другое, то возвращать строку 'Must be set!'.
"""


# my_set_left, my_set_mid и my_set_right.

def get_info_for_3_set(a, b, c):
    if type(a) != set or type(b) != set or type(c) != set:
        return 'Must be set!'
    return {
        'left == mid == right': a == b == c,
        'left == mid': a == b,
        'left == right': a == c,
        'mid == right': b == c,

        'left & mid': a.intersection(b),
        'left & right': a.intersection(c),
        'mid & right': b.intersection(c),

        'left <= mid': a.issubset(b),
        'mid <= left': a.issuperset(b),
        'left <= right': a.issubset(c),
        'right <= left': a.issuperset(c),
        'mid <= right': b.issubset(c),
        'right <= mid': c.issuperset(b)
    }
