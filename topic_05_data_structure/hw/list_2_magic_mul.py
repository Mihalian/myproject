"""
Функция magic_mul.

Принимает 1 аргумент: список my_list.

Возвращает список, который состоит из
[первого элемента my_list]
+ [три раза повторенных списков my_list]
+ [последнего элемента my_list].

Пример:
входной список [1,  'aa', 99]
результат [1, 1,  'aa', 99, 1,  'aa', 99, 1,  'aa', 99, 99].

Если вместо списка передано что-то другое, то возвращать строку 'Must be list!'.
Если список пуст, то возвращать строку 'Empty list!'.
"""


def magic_mul(my_list: list):
    if type(my_list) != list:
        return 'Must be list!'
    elif len(my_list) == 0:
        return 'Empty list!'
    li = []
    fli = my_list[:1]
    tli = my_list * 3
    lli = my_list[-1]
    li.extend(fli)
    li.extend(tli)
    li.append(lli)
    return li
print(magic_mul([2, 'aupp',3,4]))