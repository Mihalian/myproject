"""
Функция count_word.

Принимает 2 аргумента:
список слов my_list и
строку word.

Возвращает количество word в списке my_list.

Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.
Если вместо строки передано что-то другое, то возвращать строку 'Second arg must be str!'.
Если список пуст, то возвращать строку 'Empty list!'.
"""


def count_word(l: list, s: str):
    if type(l) != list:
        return "First arg must be list!"
    elif type(s) != str:
        return 'Second arg must be str!'
    elif len(l) == 0:
        return 'Empty list!'
    return l.count(s)
