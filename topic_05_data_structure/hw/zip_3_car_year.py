"""
Функция zip_car_year.

Принимает 2 аргумента: список с машинами и список с годами производства.

Возвращает список с парами значений из каждого аргумента, если один список больше другого,
то заполнить недостающие элементы строкой "???".

Подсказка: zip_longest.

Если вместо списков передано что-то другое, то возвращать строку 'Must be list!'.
Если список (хотя бы один) пуст, то возвращать строку 'Empty list!'.
"""
from itertools import zip_longest


def zip_car_year(c: list, e: list):
    if type(c) != list or type(e) != list:
        return 'Must be list!'
    elif len(c) == 0 or len(e) == 0:
        return 'Empty list!'
    return list(zip_longest(c, e, fillvalue='???'))
