"""
Функция zip_names.

Принимает 2 аргумента: список с именами и множество с фамилиями.

Возвращает список с парами значений из каждого аргумента.

Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
Если вместо set передано что-то другое, то возвращать строку 'Second arg must be set!'.

Если list пуст, то возвращать строку 'Empty list!'.
Если set пуст, то возвращать строку 'Empty set!'.

Если list и set различного размера, обрезаем до минимального (стандартный zip).
"""


def zip_names(n: list, f: set):
    if type(n) != list:
        return 'First arg must be list!'
    if type(f) != set:
        return 'Second arg must be set!'
    if len(n) == 0:
        return 'Empty list!'
    if len(f) == 0:
        return 'Empty set!'
    return list(zip(n, f))


print(zip_names(['Va', 'Ac', 'Fa', 'Ec'], {'xsdv', 'gnnf', 'zvsvsa', 'vwwwv', 'dqa', 'qqc'}))
