"""
Функция flower_with_default_vals.

Принимает 3 аргумента:
цветок (по умолчанию "ромашка"),
цвет (по умолчанию "белый"),
цена (по умолчанию 10.25).

Функция flower_with_default_vals выводит строку в формате
"Цветок: <цветок> | Цвет: <цвет> | Цена: <цена>".

При этом в функции flower_with_default_vals есть проверка на то, что цветок и цвет - это строки

(* Подсказка: type(x) == str или isinstance(s, str)), а также цена - это число больше 0, но меньше 10000.
В функции main вызвать функцию flower_with_default_vals различными способами
(перебрать варианты: цветок, цвет, цена, цветок цвет, цветок цена, цвет цена, цветок цвет цена).

(* Использовать именованные аргументы).
"""


def flower_with_default_vals(flower="ромашка", color="белый", price=10.25):
    if type(flower) != str and type(color) != str:
        return 'Введи строку'
    elif 0 < price > 1000:
        return "Цена - это число больше 0, но меньше 10000"
    else:
        return f"Цветок: {flower} | Цвет: {color} | Цена: {price}"


print(flower_with_default_vals())
print(flower_with_default_vals('Цветок', 'зеленый', 456))
print(flower_with_default_vals('Цветок', 'зеленый', 1245))
print(flower_with_default_vals(2314, 'зеленый', 456))
print(flower_with_default_vals(flower='Цветок',  price=14))