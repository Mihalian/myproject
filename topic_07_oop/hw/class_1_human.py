"""
Класс Human.

Поля:
age,
first_name,
last_name.

При создании экземпляра инициализировать поля класса.

Создать метод get_age, который возвращает возраст человека.

Перегрузить оператор __eq__, который сравнивает объект человека с другим по атрибутам.

Перегрузить оператор __str__, который возвращает строку в виде "Имя: first_name last_name Возраст: age".
"""


class Human:
    def __init__(self, age, first_name, last_name):
        self.age = age
        self.first_name = first_name
        self.last_name = last_name

    def __str__(self):
        return f"Имя: {self.first_name} {self.last_name} Возраст: {self.age}"

    def __eq__(self, other):
        # return self is other
        a = (self.age is other.age, self.first_name is other.first_name, self.last_name is other.last_name)

        return all(a)

    def get_age(self):
        return self.age


if __name__ == '__main__':
    a = Human(26, 'Petr', 'Semon')
    b = Human(26, 'Petr', 'Semon')
    print(a)
    print(a == b)
