"""
Класс School.

Поля:
список людей в школе (общий list для Pupil и Worker): people,
номер школы: number.

Методы:
get_avg_mark: вернуть средний балл всех учеников школы
get_avg_salary: вернуть среднюю зп работников школы
get_worker_count: вернуть сколько всего работников в школе
get_pupil_count: вернуть сколько всего учеников в школе
get_pupil_names: вернуть все имена учеников (с повторами, если есть)
get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
get_max_pupil_age: вернуть возраст самого старшего ученика
get_min_worker_salary: вернуть самую маленькую зп работника
get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
(список из одного или нескольких элементов)
"""

from statistics import mean
from topic_07_oop.hw.class_4_1_pupil import Pupil
from topic_07_oop.hw.class_4_2_worker import Worker


class School:
    def __init__(self, people, number):
        self.people = people
        self.number = number

    def get_pupil(self):
        return [i for i in self.people if isinstance(i, Pupil)]

    def get_worker(self):
        return [i for i in self.people if isinstance(i, Worker)]

    def get_avg_mark(self):
        pu = [i.get_avg_mark() for i in self.get_pupil()]
        return mean(pu)

    def get_avg_salary(self):
        return mean([s.salary for s in self.people if isinstance(s, Worker)])

    def get_worker_count(self):
        return len(self.get_worker())

    def get_pupil_count(self):
        return len(self.get_pupil())

    def get_pupil_names(self):
        return [i.name for i in self.get_pupil()]

    def get_unique_worker_positions(self):
        return set([i.position for i in self.get_worker()])

    def get_max_pupil_age(self):
        return max([i.age for i in self.get_pupil()])

    def get_min_worker_salary(self):
        return min([i.salary for i in self.get_worker()])

    def get_min_salary_worker_names(self):
        sp = self.get_min_worker_salary()
        return [i.name for i in self.get_worker() if i.salary == sp]

