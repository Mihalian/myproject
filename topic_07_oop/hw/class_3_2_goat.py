"""
Класс Goat.

Поля:
имя: name,
возраст: age,
сколько молока дает в день: milk_per_day.

Методы:
get_sound: вернуть строку 'Бе-бе-бе',
__invert__: реверс строки с именем (например, была Маруся, а стала Ясурам). вернуть self
__mul__: умножить milk_per_day на число. вернуть self
"""


class Goat:
    def __init__(self, name, age, milk_per_day):
        self.name = name
        self.age = age
        self.milk_per_day = milk_per_day

    def get_sound(self):
        return 'Бе-бе-бе'

    def __invert__(self):
        # return self.name[::-1]
        self.name = self.name[::-1].title()
        return self

    def __mul__(self, other):
        self.milk_per_day *= int(other)
        return self


if __name__ == '__main__':
    a = Goat('Belka', 3, 24)
    b = Goat('Tola', 5, 14)
    print(a.name)
    print(~a)
    # print(Chicken.get_info(a))
    # print('__' * 10)
    # print(Chicken.get_sound(a))
    # print('__' * 10)
    print(a.milk_per_day * 2)
